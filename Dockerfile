FROM openjdk:11
COPY ./target/journal.jar /app/
WORKDIR /app/
EXPOSE 49081
CMD java -jar /app/journal.jar
