package com.exam.journal.service;

import com.exam.journal.entity.Currency;
import com.exam.journal.entity.CurrencyValue;
import com.exam.journal.repository.CurrencyRepository;
import com.exam.journal.repository.CurrencyValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * При создании журнал запрашивает у генератора доступные валюты и сохраняет себе в справочник.
 * При старте он выбирает из справочника имеющиеся валюты и запрашивает их значения у генератора.
 */
@Service
public class JournalService {
    private static final Logger logger = LogManager.getLogger(JournalService.class);
    private final CurrencyRepository currencyRepository;
    private final CurrencyValueRepository currencyValuesRepository;
    private final GeneratorFeignClient client;
    private boolean journalIsRunning = false;

    @Autowired
    public JournalService(CurrencyRepository currencyRepository,
                          CurrencyValueRepository currencyValuesRepository,
                          GeneratorFeignClient client) {
        this.currencyRepository = currencyRepository;
        this.currencyValuesRepository = currencyValuesRepository;
        this.client = client;

        registerAvailableCurrencies();
        start();
    }

    private void registerAvailableCurrencies() {
        try {
            Iterable<String> availableCurrencies = client.getAvailableCurrencies();
            for (String currencyName : availableCurrencies) {
                Currency currency = currencyRepository.findByName(currencyName);
                if (currency == null) {
                    Currency newCurrency = new Currency();
                    newCurrency.setName(currencyName);
                    try {
                        currencyRepository.save(newCurrency);
                    } catch (Exception ex) {
                        logger.warn("Не удалось занести валюту {} в справочник", currencyName);
                    }
                    logger.info("Валюта {} успешно занесена в справочник", currencyName);
                }
            }
        } catch (Exception ex) {
            logger.warn("Не удалось получить список доступных валют от сервиса генерации");
        }
    }

     private void start() {
         if (journalIsRunning) {
             logger.warn("Журнал уже запущен");
             return;
         }

         List<Currency> currencies = StreamSupport
                 .stream(currencyRepository.findAll().spliterator(), false)
                 .collect(Collectors.toList());
         if (currencies.size() == 0) {
             logger.info("В справочнике нет ни одной валюты, запрос значений невозможен");
             return;
         }

         Thread journalThread = new Thread(() -> {
             while (true) {
                 try {
                     for (Currency currency : currencies) {
                         try {
                             LocalDateTime lastMoment = currencyValuesRepository.getCurrencyLastMoment(currency.getId());
                             List<CurrencyValueDTO> values;
                             if (lastMoment != null) {
                                 values = client.getCurrencyValuesFromMoment(currency.getName(), lastMoment.toString());
                             } else {
                                 values = client.getCurrencyValues(currency.getName());
                             }

                             for (CurrencyValueDTO value : values) {
                                 CurrencyValue currencyValue = new CurrencyValue();
                                 currencyValue.setCurrency(currency);
                                 currencyValue.setValue(value.getValue());
                                 currencyValue.setMoment(value.getMoment());
                                 try {
                                     currencyValuesRepository.save(currencyValue);
                                 } catch (Exception ex) {
                                     // Игнорируем случайные дубли
                                 }
                             }
                         } catch (Exception ex) {
                             logger.warn("Не удалось получить значение валюты {} от сервиса генерации. Причина: {}",
                                     currency.getName(), ex.getMessage());
                         }
                     }
                     Thread.sleep(10500);
                 } catch (Exception ex) {
                     logger.warn("Сервис журнала остановлен. Причина: {}", ex.getMessage());
                     return;
                 }
             }
         });
         journalThread.start();
         journalIsRunning = true;
         logger.info("Журнал запущен" + LocalDateTime.now());
     }

     public List<CurrencyValueDTO> getCurrencyValuesForPeriod(String pairName, LocalDateTime from, LocalDateTime to) {
        long currencyId;

        Currency currency = currencyRepository.findByName(pairName);
        if (currency == null) {
            logger.warn("Невозможно получить id для валюты {}", pairName);
            throw new RuntimeException("Не удалось получить id валюты");
        }
        currencyId = currency.getId();

        List<CurrencyValue> values = currencyValuesRepository.getCurrencyValuesForPeriod(currencyId, from, to);
        return values.stream()
                .map(v -> new CurrencyValueDTO(v.getMoment(), v.getValue()))
                .collect(Collectors.toList());
     }
}