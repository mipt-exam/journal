package com.exam.journal.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@FeignClient(name = "generator-client", url = "${generator}")
public interface GeneratorFeignClient {
    @GetMapping("/currency/registered")
    Iterable<String> getAvailableCurrencies();

    @GetMapping("/currency/values/{pairName}")
    List<CurrencyValueDTO> getCurrencyValues(@PathVariable String pairName);

    @GetMapping("/currency/values/criteria")
    List<CurrencyValueDTO> getCurrencyValuesFromMoment(@RequestParam String pairName, @RequestParam String from);
}