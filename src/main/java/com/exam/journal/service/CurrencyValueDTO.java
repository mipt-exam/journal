package com.exam.journal.service;

import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class CurrencyValueDTO {

    private final LocalDateTime moment;
    private final double value;

    public CurrencyValueDTO (LocalDateTime moment, double value) {
        this.moment = moment;
        this.value = value;
    }
}
