package com.exam.journal.controller;

import com.exam.journal.entity.Currency;
import com.exam.journal.entity.CurrencyValue;
import com.exam.journal.repository.CurrencyRepository;
import com.exam.journal.repository.CurrencyValueRepository;
import com.exam.journal.service.CurrencyValueDTO;
import com.exam.journal.service.JournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class JournalController {

    private final JournalService journal;
    private final CurrencyRepository currencyRepository;
    private final CurrencyValueRepository currencyValueRepository;

    @Autowired
    public JournalController(JournalService journal,
                             CurrencyRepository currencyRepository,
                             CurrencyValueRepository currencyValueRepository) {

        this.journal = journal;

        this.currencyRepository = currencyRepository;
        this.currencyValueRepository = currencyValueRepository;
    }

    @GetMapping("/journal/values/period")
    public ResponseEntity getCurrencyValuesForPeriod(@RequestParam String pairName,
                                                     @RequestParam String from,
                                                     @RequestParam String to) {
        LocalDateTime ldtFrom;
        LocalDateTime ldtTo;
        try {
            ldtFrom = LocalDateTime.parse(from);
            ldtTo = LocalDateTime.parse(to);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("Некорректный формат времени");
        }
        try {
            List<CurrencyValueDTO> result = journal.getCurrencyValuesForPeriod(pairName, ldtFrom, ldtTo);
            return ResponseEntity.ok(result);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("Не удалось получить значение валюты");
        }
    }

    @PostMapping("/postReqistretedCurrencyToAnalytic")
    public Iterable<Currency> postReqistretedCurrencyToAnalytic() {
        return currencyRepository.findAll();
    }

    @PostMapping("/postJournalDataBaseToAnalytic")
    public Iterable<CurrencyValue> postJournalDataBaseToAnalytic() {
        return currencyValueRepository.findAll();
    }

}