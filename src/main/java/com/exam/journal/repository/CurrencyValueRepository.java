package com.exam.journal.repository;

import com.exam.journal.entity.CurrencyValue;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;


public interface CurrencyValueRepository extends CrudRepository<CurrencyValue, Long> { // репозиторий для работы с журналом цен
    @Query(value = "select * from currency_value where currency_id = :currencyId and moment >= :from and moment <= :to", nativeQuery = true)
    List<CurrencyValue> getCurrencyValuesForPeriod(long currencyId, LocalDateTime from, LocalDateTime to);

    @Query(value = "select moment from currency_value where currency_id = :currencyId order by moment desc limit 1", nativeQuery = true)
    LocalDateTime getCurrencyLastMoment(long currencyId);
}
