package com.exam.journal.repository;

import com.exam.journal.entity.Currency;
import org.springframework.data.repository.CrudRepository;

public interface CurrencyRepository extends CrudRepository<Currency, Long> {
    Currency findByName(String name);
}
