package com.exam.journal.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity(name = "currency_value")
@Getter @Setter
public class CurrencyValue extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "currency_values_entity_id_seq")
    private long id;

    @Column(name = "value")
    private double value;

    @Column(name = "moment")
    private LocalDateTime moment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id")
    private Currency currency;

    @Override
    public int hashCode() {
        return Objects.hash(id, value, moment);
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj))
            return false;
        var currencyValuesEntity = (CurrencyValue) obj;
        return Objects.equals(id, currencyValuesEntity.id);
    }
}