package com.exam.journal.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "currency")
@Getter @Setter
public class Currency extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "currency_entity_id_seq")
    private long id;

    @Column(name = "name")
    private String name;

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj))
            return false;
        var currency = (Currency) obj;
        return Objects.equals(id, currency.id);
    }
}