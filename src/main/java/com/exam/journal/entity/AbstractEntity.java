package com.exam.journal.entity;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class AbstractEntity {
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || obj.getClass() != this.getClass())
            return false;
        return true;
    }
}
